import type { Course } from '@/types'
import { defineStore } from 'pinia'

interface State {
  courses: Array<Course>
}

export const useCoursesStore = defineStore('courses', {
  state: () => {
    const state: State = { courses: [] }
    return state
  },
  getters: {
    getCourseById: (state) => {
      return (courseId: number) => state.courses.find((course) => course.id === courseId)
    }
  },
  actions: {
    async refresh() {
      const apiUrl = import.meta.env.VITE_API_URL
      const response = await fetch(`${apiUrl}/courses.json`)
      const data = await response.json()
      this.courses = data
    }
  },
  persist: true
})
